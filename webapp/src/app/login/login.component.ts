import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormsModule, Validators } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule 
  ],
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent {

  @Output() public activarCalculo = new EventEmitter() ;
  username: string = '';
  password: string = '';
  error = false;

  constructor (private formBulder: FormBuilder){
  }

  loginForm = this.formBulder.group({
    usuario: [null, [Validators.required]],
    password: [null, [Validators.required]]
  })

  /**
   * método para realizar el login básico
   * emite un valor true para activar otro componente
   * saca un texto de error si no es correcto el login
   */
  login() {
    if (this.username === 'demo' && this.password === 'demo') {
      this.activarCalculo.emit(true);
    } else {
      this.error = true;
    }
  }
}
