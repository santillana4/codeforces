import { Component, Input } from '@angular/core';
import { NumeroMayorService } from './numero-mayor.service';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';

@Component({
  selector: 'app-numero-mayor',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule
  ],
  templateUrl: './numero-mayor.component.html',
  styleUrl: './numero-mayor.component.scss'
})
export class NumeroMayorComponent {
  @Input() usuario: string = '';

  formData = {
    x: 0,
    y: 0,
    n: 0
  };

  solution: any = null;
  enunciado = 'Dado tres números enteros x, y, n. Encontrar el máximo entero k tal que:';
  condicion = '0 ≤ k ≤ n y que k % x = y';

  constructor(private numeroMayorService: NumeroMayorService) { }

  /**
   * Método que llama al servicio para una llamada post
   */
  numeroMayorPost() {
    /* this.numeroMayorService.numeroMayorPost(this.formData.x, this.formData.y, this.formData.n)
       .toPromise().then( rest => this.solution = rest).catch(
         error => console.error('Error solving problem with GET:', error));  */
     this.solution = this.obtenerValor(this.formData.x, this.formData.y, this.formData.n);
  }

  /**
   * Método que llama al servicio para una llamada get
   */
  numeroMayorGet() {
    /* this.numeroMayorService.numeroMayorGet(this.formData.x, this.formData.y, this.formData.n)
    .subscribe(data => { this.solution = data.solution; },
      error => { console.error('Error solving problem with GET:', error); }
    ); */
    this.solution = this.obtenerValor(this.formData.x, this.formData.y, this.formData.n);
  }

  /**
   * método para reiniciar los valores
   */
  reiniciarValores() {
    this.formData.x = 0;
    this.formData.y = 0;
    this.formData.n = 0;

    this.solution = null;
  }

  /**
   * método para encontrar el valor de k 
   * @param x 
   * @param y 
   * @param n 
   * @returns 
   */
  obtenerValor(x: number, y: number, n: number) {
    let k = 0;
    for (let i = 0; i < n; i++) {
      //comprobar si cumple la condición k % x = y
      if (i % x === y) {
        k = i;
      }
    }
    return k;
  }
}
