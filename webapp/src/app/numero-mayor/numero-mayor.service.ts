import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NumeroMayorService {

  private baseUrl = 'http://localhost:8080/api';

  //constructor(private http: HttpClient) { }

  /**
   * Método para hacer la llamada post al back
   * @param x 
   * @param y 
   * @param n 
   * @returns 
   */
  /* numeroMayorPost(x: number, y: number, n: number): Observable<any> {
    const url = `${this.baseUrl}/numeroMayor`;
    const body = { x, y, n };
    return this.http.post<any>(url, body);
  } */

  /**
   * Método para hacer la llamada get al back
   * @param x 
   * @param y 
   * @param n 
   * @returns 
   */
  /* numeroMayorGet(x: number, y: number, n: number): Observable<any> {
    const url = `${this.baseUrl}/numeroMayor?x=${x}&y=${y}&n=${n}`;
    return this.http.get<any>(url);
  } */
}
