import { TestBed } from '@angular/core/testing';

import { NumeroMayorService } from './numero-mayor.service';

describe('NumeroMayorService', () => {
  let service: NumeroMayorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NumeroMayorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
