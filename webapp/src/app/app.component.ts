import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { NumeroMayorComponent } from './numero-mayor/numero-mayor.component';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { HttpClientModule } from '@angular/common/http';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    RouterOutlet,
    NumeroMayorComponent,
    LoginComponent,
    CommonModule,
    MatIconModule,
    MatButtonModule,
    HttpClientModule     
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  title = 'Problema1374a';

  usuario: any;
  activarCalculo = false;
  activarLogin = true;

  activarCalculoFuncion(event: any) {
    if (event === true) {
      this.activarCalculo = true;
      this.activarLogin = false;
    } else {
      this.activarCalculo = false;
      this.activarLogin = true;
    }
  }

  cerrarSesion() {
    this.activarCalculo = false;
    this.activarLogin = true;
  }



}
