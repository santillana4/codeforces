package com.santillana.codeforces.bean;

import javax.validation.constraints.Min;

/**
 * Clase que continen los elementos del bean
 */
public class NumeroMayor {
    
    @Min(0)
    private int x;

    @Min(0)
    private int y;

    @Min(0)
    private int n;
    
    // Getters and setters
    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }
}
