package com.santillana.codeforces.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

import com.santillana.codeforces.bean.NumeroMayor;
import com.santillana.codeforces.service.NumeroMayorService;

/**
 * Clase entrada de la Api con métodos get y post que se llaman desde el front
 */
@RestController
@RequestMapping("/api")
@Tag(name = "Obtener el número máximo", description = "Endpoints para obtener el número máximo")
public class NumeroMayorController {

	@Autowired
	public static NumeroMayorService numeroMayorService;

    /**
     * Método get para devolver el resultado buscado
     * @param x
     * @param y
     * @param n
     * @return k
     */
    @CrossOrigin(origins = "http://localhost:4200")
    @Operation(summary = "Obtener resultado con GET", description = "Obtiene el resultado basado en x, y, n", tags = "Obtener el número máximo")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Operación exitosa"),
        @ApiResponse(responseCode = "400", description = "Solicitud inválida"),
        @ApiResponse(responseCode = "500", description = "Error interno del servidor")
    })
    @GetMapping("/numeroMayor")
    public ResponseEntity<Integer> numeroMayorGet(@RequestParam int x, @RequestParam int y, @RequestParam int n) {
        int k = numeroMayorService.numeroMayor(x, y, n);
        return ResponseEntity.ok(k);
    }

    /**
     * Método post para devolver el resultado buscado
     * @param request
     * @return
     */
    @CrossOrigin(origins = "http://localhost:4200")
    @Operation(summary = "Obtener resultado con GET", description = "Obtiene el resultado basado en x, y, n", tags = "Obtener el número máximo")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Operación exitosa"),
        @ApiResponse(responseCode = "400", description = "Solicitud inválida"),
        @ApiResponse(responseCode = "500", description = "Error interno del servidor")
    })
    @PostMapping("/numeroMayor")
    public ResponseEntity<Integer> numeroMayorPost(@RequestBody @Validated NumeroMayor request) {
        int k = numeroMayorService.numeroMayor(request.getX(), request.getY(), request.getN());
        return ResponseEntity.ok(k);
    }
}
