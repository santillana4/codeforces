package com.santillana.codeforces.utils;

import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;

@Configuration
public class SwaggerConfig {

    @Bean
    public GroupedOpenApi codeforcesApi() {
        return GroupedOpenApi.builder()
                .group("codeforces")
                .pathsToMatch("/api/numeroMayor")
                .build();
    }

    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI()
            .info(new Info()
                .title("API de Codeforces")
                .description("Documentación de la API de Codeforces")
                .version("1.0.0"));
    }
}
