package com.santillana.codeforces.service;

/**
 * Clase para recuperar el número máximo
 */
public class NumeroMayorService {

    /**
     * Método para entontrar el resultado de la operación
     * 
     * @param x
     * @param y
     * @param n
     * @return
     */
    public static int numeroMayor(int x, int y, int n) {
        int k = 0;
        for (int i = 0; i < n; i++) {
            // comprobar si cumple la condición k % x = y
            if (i % x == y) {
                k = i;
            }
        }
        return k;
    }
}
