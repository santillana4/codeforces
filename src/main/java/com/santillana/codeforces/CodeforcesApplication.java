package com.santillana.codeforces;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.santillana.codeforces.service.NumeroMayorService;

@SpringBootApplication
public class CodeforcesApplication {

	public static void main(String[] args) {
		int k = NumeroMayorService.numeroMayor(7,5,12345);
		System.out.println("K es: " + k);
	}

}
