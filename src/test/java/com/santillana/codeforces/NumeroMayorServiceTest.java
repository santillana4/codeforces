package com.santillana.codeforces;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.santillana.codeforces.service.NumeroMayorService;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class NumeroMayorServiceTest {

    @Autowired
    public static NumeroMayorService numeroMayorService;

    @Test
    public void testNumeroMayor() {
        // Test 1
        int resultado = numeroMayorService.numeroMayor(7, 5, 12345);
        assertEquals(12339, resultado, "El resultado debe ser 12339");

        // Test 2
        resultado = numeroMayorService.numeroMayor(10, 2, 100);
        assertEquals(92, resultado, "El resultado debe ser 92");

        // Test 3
        resultado = numeroMayorService.numeroMayor(3, 1, 10);
        assertEquals(7, resultado, "El resultado debe ser 7");
    }
}
